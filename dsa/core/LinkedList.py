class Node:
    def __init__(self, value):
        self.value = value
        self.next = None


class LinkedList:
    def __init__(self, value):
        # Create First Node
        self.head = Node(value)
        self.tail = self.head
        self.length = 1

    def print_list(self):
        print("Linked List: ", end="")
        temp_node = self.head
        while temp_node is not None:
            print(temp_node.value, end=" ")
            temp_node = temp_node.next
        print(f"; Len: {self.length}")

    def append(self, value):
        temp_node = Node(value)
        # If Linked List is empty
        if self.head is None:
            self.head = temp_node
            self.tail = temp_node
            self.length = 1
        else:
            last_node = self.tail
            last_node.next = temp_node
            self.tail = temp_node
            self.length = self.length + 1
        return True

    def pop(self):
        if self.length < 1:
            return None
        last_node = self.tail
        if self.length == 1:
            self.head = self.tail = None
        else:
            curr = self.head
            while curr.next is not self.tail:
                curr = curr.next
            self.tail = curr
            self.tail.next = None
        self.length -= 1
        return last_node

    # def pop(self):
    #     # If Linked List is empty
    #     if self.head is None:
    #         return None
    #     # If Linked List has only one Node
    #     elif self.head == self.tail:
    #         temp_node = self.head
    #         self.head = None
    #         self.tail = None
    #         self.length -= 1
    #         return temp_node
    #     # Normal scenaio
    #     else:
    #         temp_node = self.head
    #         while temp_node is not None:
    #             if temp_node.next is self.tail:
    #                 last_node = self.tail
    #                 temp_node.next = None
    #                 self.tail = temp_node
    #             temp_node = temp_node.next
    #         self.length -= 1
    #         return last_node

    def prepend(self, value):
        new_node = Node(value)
        # If Linked List is empty
        if self.length == 0:
            self.head = new_node
            self.tail = new_node
        # Normal scenaio
        else:
            new_node.next = self.head
            self.head = new_node
        self.length += 1
        return True

    def popfirst(self):
        # If Linked List is empty
        if self.length == 0:
            return None
        # If Linked list has single item
        elif self.head is self.tail:
            temp_node = self.head
            self.head = None
            self.tail = None
            self.length -= 1
            return temp_node
        else:
            first_node = self.head
            self.head = self.head.next
            first_node.next = None
            self.length -= 1
            return first_node

    def get(self, index):
        if index < 0:
            return None
        if index + 1 > self.length:
            return None
        else:
            temp_node = self.head
            for _ in range(index):
                temp_node = temp_node.next
        return temp_node

    def set_value(self, index, value):
        temp_node = self.get(index)
        if temp_node:
            temp_node.value = value
            return True
        return False

    def insert(self, index, value):
        if index < 0 or index > self.length:
            return False
        elif index == 0:
            return self.prepend(value)
        elif index == self.length:
            return self.append(value)
        else:
            new_node = Node(value)
            new_node.next = self.get(index)
            self.get(index - 1).next = new_node
            self.length += 1
            return True

    def remove(self, value):
        if self.length < 1:
            return None
        if self.head.value == value:
            return self.popfirst()

        if self.tail.value == value:
            return self.pop()

        intrested_node = None
        curr = self.head
        while curr.next:
            if curr.next.value == value:
                temp = curr
                intrested_node = curr.next
                curr.next = intrested_node.next
                intrested_node.next = None
                curr = temp
                self.length -= 1
                break
            curr = curr.next

        return intrested_node

    # def remove(self, index):
    #     if index < 0 or index >= self.length:
    #         return None
    #     elif index == 0:
    #         return self.popfirst()
    #     elif index == self.length - 1:
    #         return self.pop()
    #     else:
    #         prev_node = self.get(index - 1)
    #         current_node = prev_node.next
    #         prev_node.next = current_node.next
    #         current_node.next = None
    #         self.length -= 1
    #         return current_node

    def reverse(self):
        if self.length <= 1:
            return
        else:
            temp = self.head
            self.head = self.tail
            self.tail = temp
            prev_node = None
            next_node = temp.next
            while next_node is not None:
                temp.next = prev_node
                prev_node = temp
                temp = next_node
                next_node = next_node.next
            self.head.next = prev_node


# Test for pop
# my_linked_list = LinkedList(1)
# my_linked_list.append(2)
# my_linked_list.append(3)
# my_linked_list.print_list()
# print("pop: ", my_linked_list.pop().value)
# my_linked_list.print_list()
# print("pop: ", my_linked_list.pop().value)
# my_linked_list.print_list()
# print("pop: ", my_linked_list.pop().value)
# my_linked_list.print_list()
# print("pop: ", my_linked_list.pop())
# my_linked_list.print_list()
# print("pop: ", my_linked_list.pop())
# my_linked_list.print_list()

# Test for prepend
# my_linked_list = LinkedList(1)
# my_linked_list.append(2)
# my_linked_list.print_list()
# print("pop first: ", my_linked_list.popfirst().value)
# my_linked_list.print_list()
# print("pop first: ", my_linked_list.popfirst().value)
# my_linked_list.print_list()
# print("pop first: ", my_linked_list.popfirst())
# my_linked_list.print_list()
# print("head: ", my_linked_list.head.value)
# print("tail: ", my_linked_list.tail.value)

# Test for get
# my_linked_list = LinkedList(1)
# my_linked_list.append(2)
# my_linked_list.append(3)
# my_linked_list.append(4)
# my_linked_list.print_list()
# print("value at index 2:", my_linked_list.get(2).value)
# print("value at index 1:", my_linked_list.get(1).value)
# print("value at index 0:", my_linked_list.get(0).value)
# print("value at index -1:", my_linked_list.get(-1))
# print("value at index 4:", my_linked_list.get(4))

# Test for set
# my_linked_list = LinkedList(0)
# my_linked_list.append(1)
# my_linked_list.append(3)
# my_linked_list.append(3)
# my_linked_list.print_list()
# print("set value 4 in index 3:", my_linked_list.set_value(3, 4))
# my_linked_list.print_list()
# print("set value 5 in index 0:", my_linked_list.set_value(0, 5))
# my_linked_list.print_list()
# print("set value 0 in index -1:", my_linked_list.set_value(-1, 0))
# my_linked_list.print_list()
# print("set value 0 in index 4:", my_linked_list.set_value(4, 0))
# my_linked_list.print_list()

# Test for insert
# my_linked_list = LinkedList(7)
# my_linked_list.append(1)
# my_linked_list.append(2)
# my_linked_list.append(4)
# my_linked_list.print_list()
# print("insert 3 at index 3: ", my_linked_list.insert(3, 3))
# my_linked_list.print_list()
# print("insert 5 at index 5: ", my_linked_list.insert(5, 5))
# my_linked_list.print_list()
# print("insert 0 at index 0: ", my_linked_list.insert(0, 0))
# my_linked_list.print_list()
# print("pop: ", my_linked_list.pop().value)
# print("pop: ", my_linked_list.pop().value)
# print("pop: ", my_linked_list.pop().value)
# print("pop: ", my_linked_list.pop().value)
# print("pop: ", my_linked_list.pop().value)
# print("pop: ", my_linked_list.pop().value)
# print("pop: ", my_linked_list.pop().value)
# my_linked_list.print_list()
# print("insert 0 at index 0: ", my_linked_list.insert(0, 0))
# my_linked_list.print_list()
# print("insert -1 at index 0: ", my_linked_list.insert(0, -1))
# my_linked_list.print_list()
# print("insert 1 at index 2: ", my_linked_list.insert(2, 1))
# my_linked_list.print_list()
# print("insert 2 at index 1: ", my_linked_list.insert(1, 2))
# my_linked_list.print_list()
# print("insert 8 at index 5: ", my_linked_list.insert(5, 8))
# my_linked_list.print_list()
# print("insert 8 at index -1: ", my_linked_list.insert(-1, 8))
# my_linked_list.print_list()

# Test for remove
# my_linked_list = LinkedList(0)
# my_linked_list.append(1)
# my_linked_list.append(2)
# my_linked_list.append(3)
# my_linked_list.append(4)
# my_linked_list.print_list()
# print("remove at index 4: ", my_linked_list.remove(4).value)
# my_linked_list.print_list()
# print("remove at index 2: ", my_linked_list.remove(2).value)
# my_linked_list.print_list()
# print("remove at index 0: ", my_linked_list.remove(0).value)
# my_linked_list.print_list()
# print("remove at index 2: ", my_linked_list.remove(2))
# my_linked_list.print_list()
# print("remove at index -1: ", my_linked_list.remove(-1))
# my_linked_list.print_list()
# print("remove at index 0: ", my_linked_list.remove(0).value)
# my_linked_list.print_list()
# print("remove at index 0: ", my_linked_list.remove(0).value)
# my_linked_list.print_list()
# print("remove at index 0: ", my_linked_list.remove(0))
# my_linked_list.print_list()
#
# Test for reverse
# my_linked_list = LinkedList(1)
# my_linked_list.print_list()
# my_linked_list.reverse()
# my_linked_list.append(8)
# my_linked_list.print_list()
# my_linked_list.reverse()
# my_linked_list.print_list()

