from collections import deque


class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None


class BinarySearchTree:
    def __init__(self):
        self.root = None

    def insert(self, value):
        new_node = Node(value)
        if self.root is None:
            self.root = new_node
            return True

        curr = self.root
        while True:
            if new_node.value == curr.value:
                return False
            if new_node.value < curr.value:
                if curr.left is None:
                    curr.left = new_node
                    return True
                curr = curr.left
            else:
                if curr.right is None:
                    curr.right = new_node
                    return True
                curr = curr.right

    def contains(self, value):
        curr = self.root
        while curr:
            if curr.value == value:
                return True
            if value < curr.value:
                curr = curr.left
            else:
                curr = curr.right

        return False

    def BFS(self):
        que = deque()
        result = []
        if self.root:
            que.append(self.root)
        while que:
            node = que.popleft()
            result.append(node.value)
            if node.left:
                que.append(node.left)
            if node.right:
                que.append(node.right)

    def dfs_pre_order(self):
        result = []

        def traverse(current_node):
            result.append(current_node.value)

            if current_node.left:
                traverse(current_node.left)

            if current_node.right:
                traverse(current_node.right)

        traverse(self.root)
        return result

    def dfs_post_order(self):
        result = []

        def traverse(current_node):
            if current_node.left:
                traverse(current_node.left)

            if current_node.right:
                traverse(current_node.right)

            result.append(current_node.value)

        traverse(self.root)
        return result

    def dfs_in_order(self):
        result = []

        def traverse(current_node):
            if current_node.left:
                traverse(current_node.left)

            result.append(current_node.value)

            if current_node.right:
                traverse(current_node.right)

        traverse(self.root)
        return result

    def something(self):
        from collections import deque

        q = deque()
