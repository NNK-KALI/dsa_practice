class Node:
    def __init__(self, value):
        self.prev = None
        self.value = value
        self.next = None


class DoublyLinkedList:
    def __init__(self, value):
        new_node = Node(value)
        self.head = new_node
        self.tail = new_node
        self.length = 1

    def print_list(self):
        curr = self.head
        print("DLL: ", end="")
        while curr:
            print(curr.value, end=" ")
            curr = curr.next
        print(" ;Len: ", self.length)

    def append(self, value):
        new_node = Node(value)
        if self.length < 1:
            self.head = new_node
            self.tail = new_node
            self.length = 1
        else:
            self.tail.next = new_node
            new_node.prev = self.tail
            self.tail = new_node
            self.length += 1
        return True

    def pop(self):
        if self.length < 1:
            return None
        last_node = self.tail
        if self.length == 1:
            self.head = None
            self.tail = None
        else:
            self.tail = last_node.prev
            last_node.prev.next = None
            last_node.prev = None
        self.length -= 1
        return last_node

    def prepend(self, value):
        # if list is empty
        new_node = Node(value)
        if self.length < 1:
            self.head = new_node
            self.tail = new_node
            self.length = 1
        # if list is not empty
        else:
            self.head.prev = new_node
            new_node.next = self.head
            self.head = new_node
            self.length += 1
        return True

    def pop_first(self):
        # if list is empty
        if self.length < 1:
            return None
        # if list has one Node
        first_node = self.head
        if self.length == 1:
            self.head = None
            self.tail = None
        # if list has more than one node
        else:
            self.head = self.head.next
            self.head.prev = None
            first_node.next = None
        self.length -= 1
        return first_node

    def get(self, index):
        if index < 0 or index >= self.length:
            return None
        if index < self.length // 2:
            current_node = self.head
            for _ in range(index):
                current_node = current_node.next
        else:
            current_node = self.tail
            for _ in range(self.length - index - 1):
                current_node = current_node.prev

        return current_node

    def set(self, index, value):
        temp = self.get(index)
        if not temp:
            return False
        temp.value = value
        return True

    def insert(self, index, value):
        if index < 0 or index > self.length:
            return False
        if index == 0:
            return self.prepend(value)
        if index == self.length:
            return self.append(value)

        new_node = Node(value)
        temp = self.get(index)
        new_node.next = temp
        new_node.prev = temp.prev
        temp.prev.next = new_node
        temp.prev = new_node

        self.length += 1
        return True

    def remove(self, index):
        if index < 0 or index >= self.length:
            return None
        if index == 0:
            return self.pop_first()
        if index == self.length - 1:
            return self.pop()

        temp = self.get(index)
        temp.prev.next = temp.next
        temp.next.prev = temp.prev
        temp.prev = None
        temp.next = None
        self.length -= 1
        return temp
