class Node:
    def __init__(self, value):
        self.value = value
        self.next = None


class Queue:
    def __init__(self, value):
        new_node = Node(value)
        self.first = new_node
        self.last = new_node
        self.length = 1

    def print_queue(self):
        print("Queue: ", end="")
        curr = self.first
        while curr:
            print(curr.value, end=" ")
            curr = curr.next
        print(" ; Len: ", self.length)

    def enqueue(self, value):
        new_node = Node(value)
        if self.length < 1:
            self.first = self.last = new_node
            self.length = 1
            return

        self.last.next = new_node
        self.last = new_node
        self.length += 1

    def dequeue(self):
        if self.length < 1:
            return None
        temp = self.first
        if self.length == 1:
            self.first = self.last = None
        else:
            self.first = self.first.next
            temp.next = None
        self.length -= 1
        return temp
