class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None


class RecursiveBST:

    def __init__(self):
        self.root = None

    def _r_insert(self, current_node, value):
        if current_node is None:
            return Node(value)

        if value < current_node.value:
            current_node.left = self._r_insert(current_node.left, value)

        if value > current_node.value:
            current_node.right = self._r_insert(current_node.right, value)

        return current_node

    def __r_contains(self, current_node, value):
        if current_node is None:
            return False

        if value == current_node.value:
            return True

        if value < current_node.value:
            return self.__r_contains(current_node.left, value)

        if value > current_node.value:
            return self.__r_contains(current_node.right, value)

    def _r_remove(self, current_node, value):
        if current_node is None:
            return None

        if current_node.value == value:
            if current_node.left and current_node.right:
                sub_tree_min = self.min_value(current_node.right)
                current_node.value = sub_tree_min
                current_node.right = self._r_remove(current_node.right, sub_tree_min)
                return current_node

            if current_node.left is None and current_node.right is None:
                return None
            if current_node.left:
                return current_node.left
            if current_node.right:
                return current_node.right

        if value < current_node.value:
            current_node.left = self._r_remove(current_node.left, value)

        if value > current_node.value:
            current_node.right = self._r_remove(current_node.right, value)

        return current_node

    def min_value(self, current_node):
        while current_node.left is not None:
            current_node = current_node.left
        return current_node.value

    def r_contains(self, value):
        return self.__r_contains(self.root, value)

    def r_insert(self, value):
        if self.root is None:
            self.root = Node(value)
            return

        self._r_insert(self.root, value)

    def r_remove(self, value):
        self._r_remove(self.root, value)


# my_tree = RecursiveBST()
# my_tree.r_insert(2)
# my_tree.r_insert(1)
# my_tree.r_insert(3)
#
# """
#        2
#       / \
#      1   3
# """
#
# print("root:", my_tree.root.value)
# print("root.left =", my_tree.root.left.value)
# print("root.right =", my_tree.root.right.value)
#
#
# my_tree.r_remove(2)
#
# """
#        3
#       / \
#      1   None
# """
#
#
# print("\nroot:", my_tree.root.value)
# print("root.left =", my_tree.root.left.value)
# print("root.right =", my_tree.root.right)
#
