class HashTable:
    def __init__(self, size=7):
        self.data_map = [None] * size

    def __hash(self, key):
        my_hash = 0
        for letter in key:
            my_hash = (my_hash + ord(letter) * 23) % len(self.data_map)
        return my_hash

    def set_item(self, key, value):
        address = self.__hash(key)
        if self.data_map[address] is None:
            self.data_map[address] = []
        self.data_map[address].append([key, value])

    def get(self, key):
        address = self.__hash(key)
        if self.data_map[address] is not None:
            for i in range(len(self.data_map[address])):
                if self.data_map[address][i][0] == key:
                    return self.data_map[address][i][1]
        return None

    def keys(self):
        all_keys = []
        for address in range(len(self.data_map)):
            if self.data_map[address] is not None:
                for item in range(len(self.data_map[address])):
                    all_keys.append(self.data_map[address][item][0])

        return all_keys

    def print_table(self):
        for i, val in enumerate(self.data_map):
            print(i, ": ", val)
