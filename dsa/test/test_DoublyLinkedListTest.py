import unittest
from dsa.core.DoublyLinedList import DoublyLinkedList


class TestDoublyLinkedList(unittest.TestCase):

    def test_constructor(self):
        dll = DoublyLinkedList(1)

        # Check if the node has correct value
        self.assertEqual(1, dll.head.value)

        # Check if the prev of first node is None
        self.assertEqual(None, dll.head.prev)

        # Check if the next of the node is None
        self.assertEqual(None, dll.head.next)

        # Check if head and tail are at same node
        self.assertEqual(dll.head, dll.tail)

        # Check for correct length of the linked list
        self.assertEqual(1, dll.length)

    def test_append(self):
        dll = DoublyLinkedList(1)

        # Append if list is empty
        dll.head = None
        dll.tail = None
        dll.length = 0
        self.assertEqual(True, dll.append(2))
        self.assertEqual(2, dll.head.value)
        self.assertFalse(dll.head.prev)
        self.assertFalse(dll.head.next)
        self.assertEqual(1, dll.length)

        # Append is list alredy has elements
        self.assertTrue(dll.append(4))
        self.assertEqual(4, dll.tail.value)
        self.assertEqual(2, dll.length)
        self.assertFalse(dll.tail.next)
        self.assertTrue(dll.tail.prev)

    def test_pop(self):
        dll = DoublyLinkedList(1)
        dll.append(2)
        dll.append(3)
        dll.append(4)

        # pop empty dll
        dll.head = None
        dll.tail = None
        dll.length = 0

        self.assertFalse(dll.pop())
        self.assertFalse(dll.length)

        # pop when list has one element
        dll.append(1)
        self.assertEqual(1, dll.pop().value)
        self.assertLess(dll.length, 1)
        self.assertFalse(dll.head)
        self.assertFalse(dll.tail)

        # pop when list has more than one elements
        dll.append(1)
        dll.append(2)
        dll.append(3)
        self.assertEqual(3, dll.pop().value)
        self.assertTrue(dll.tail)
        self.assertEqual(2, dll.tail.value)
        self.assertFalse(dll.tail.next)

        # pop all the nodes
        self.assertEqual(2, dll.pop().value)
        self.assertEqual(1, dll.pop().value)
        self.assertFalse(dll.head)
        self.assertFalse(dll.tail)
        self.assertFalse(dll.length)

    def test_prepend(self):
        dll = DoublyLinkedList(1)
        dll.append(2)
        dll.append(3)
        dll.append(4)

        # prepend if list is empty
        dll.head = None
        dll.tail = None
        dll.length = 0

        self.assertTrue(dll.prepend(1))
        self.assertEqual(1, dll.head.value)
        self.assertEqual(dll.head, dll.tail)
        self.assertFalse(dll.head.next)
        self.assertFalse(dll.head.next)
        self.assertEqual(1, dll.length)

        del dll

        # prepend when list is not empty
        dll = DoublyLinkedList(1)
        dll.append(2)
        dll.append(3)
        dll.append(4)

        temp_dll_len = dll.length
        self.assertTrue(dll.prepend(0))
        self.assertEqual(0, dll.head.value)
        self.assertFalse(dll.head.prev)
        self.assertTrue(dll.head.next)
        self.assertEqual(temp_dll_len + 1, dll.length)

    def test_pop_first(self):
        # if dll is empty
        dll = DoublyLinkedList(1)
        dll.head = None
        dll.tail = None
        self.length = 0

        self.assertIsNone(dll.pop_first())
        self.assertIsNone(dll.head)
        self.assertIsNone(dll.tail)
        self.assertLess(dll.length, 1)

        del dll
        # if dll has single node
        dll = DoublyLinkedList(1)

        self.assertEqual(1, dll.pop_first().value)
        self.assertFalse(dll.head)
        self.assertFalse(dll.tail)
        self.assertLess(dll.length, 1)
        del dll

        # if dll has more than single node
        dll = DoublyLinkedList(1)
        dll.append(2)
        dll.append(3)
        dll.append(4)

        temp_node = dll.pop_first()
        self.assertEqual(temp_node.value, 1)
        self.assertFalse(temp_node.prev)
        self.assertFalse(temp_node.next)
        self.assertEqual(dll.length, 3)

        self.assertEqual(2, dll.pop_first().value)
        self.assertEqual(dll.length, 2)
        self.assertEqual(3, dll.pop_first().value)

        self.assertEqual(4, dll.pop_first().value)
        self.assertFalse(dll.head)
        self.assertFalse(dll.tail)
        self.assertLess(dll.length, 1)

        self.assertIsNone(dll.pop_first())
        self.assertIsNone(dll.head)
        self.assertIsNone(dll.tail)
        self.assertLess(dll.length, 1)

    def test_get(self):
        dll = DoublyLinkedList(1)
        dll.append(2)
        dll.append(3)
        dll.append(4)

        self.assertIsNone(dll.get(-1))
        self.assertEqual(dll.get(0).value, 1)
        self.assertEqual(dll.get(1).value, 2)
        self.assertEqual(dll.get(2).value, 3)
        self.assertEqual(dll.get(3).value, 4)
        self.assertIsNone(dll.get(4))

        dll.append(5)
        self.assertEqual(5, dll.get(4).value)

    def test_set(self):
        dll = DoublyLinkedList(1)
        dll.append(2)
        dll.append(3)
        dll.append(4)

        self.assertFalse(dll.set(-1, -1))

        self.assertTrue(dll.set(0, 0))
        self.assertEqual(0, dll.head.value)

        self.assertTrue(dll.set(2, 8))
        self.assertEqual(8, dll.head.next.next.value)

        self.assertTrue(dll.set(3, 6))
        self.assertEqual(6, dll.tail.value)

        self.assertFalse(dll.set(4, 5))

    def test_insert(self):
        dll = DoublyLinkedList(1)
        dll.append(2)
        dll.append(3)
        dll.append(4)

        self.assertFalse(dll.insert(-1, 0))

        self.assertTrue(dll.insert(0, 0))
        self.assertEqual(0, dll.head.value)
        self.assertEqual(5, dll.length)
        self.assertIsNone(dll.head.prev)
        self.assertIsNotNone(dll.head.next)

        self.assertTrue(dll.insert(dll.length, 9))
        self.assertEqual(9, dll.tail.value)
        self.assertEqual(6, dll.length)
        self.assertIsNone(dll.tail.next)
        self.assertIsNotNone(dll.tail.prev)

        self.assertTrue(dll.insert(2, 10))
        self.assertEqual(10, dll.head.next.next.value)

        self.assertFalse(dll.insert(8, 11))

    def test_remove(self):
        dll = DoublyLinkedList(1)
        dll.append(2)
        dll.append(3)
        dll.append(4)

        self.assertIsNone(dll.remove(-1))

        self.assertEqual(1, dll.remove(0).value)
        self.assertEqual(3, dll.length)
        self.assertIsNone(dll.head.prev)
        self.assertIsNotNone(dll.head.next)

        self.assertEqual(3, dll.remove(1).value)
        self.assertEqual(2, dll.length)

        self.assertEqual(4, dll.remove(1).value)
        self.assertEqual(1, dll.length)

        self.assertIsNone(dll.remove(1))

        self.assertEqual(2, dll.remove(0).value)
        self.assertIsNone(dll.head)
        self.assertIsNone(dll.tail)


if __name__ == "__main__":
    unittest.main(verbosity=2)
