import unittest
from dsa.core.Queue import Queue


class QueueTest(unittest.TestCase):

    def test_constructor(self):
        q = Queue(1)

        self.assertEqual(1, q.first.value)
        self.assertIs(q.first, q.last)
        self.assertIsNone(q.first.next)
        self.assertEqual(1, q.length)

    def test_enqueue(self):
        q = Queue(1)

        q.enqueue(2)
        self.assertEqual(2, q.last.value)
        self.assertEqual(2, q.length)
        self.assertIsNone(q.last.next)

        # test for empty queue
        q.first = q.last = None
        q.length = 0

        q.enqueue(10)
        self.assertEqual(10, q.last.value)
        self.assertEqual(1, q.length)
        self.assertIsNone(q.last.next)
        self.assertIs(q.first, q.last)

        q.enqueue(2)
        self.assertEqual(2, q.last.value)
        self.assertEqual(2, q.length)
        self.assertIsNone(q.last.next)

    def test_dequeue(self):
        q = Queue(1)
        q.enqueue(2)
        q.enqueue(3)

        self.assertEqual(1, q.dequeue().value)
        self.assertEqual(2, q.first.value)
        self.assertEqual(2, q.length)

        self.assertEqual(2, q.dequeue().value)
        self.assertEqual(3, q.first.value)
        self.assertEqual(1, q.length)

        self.assertEqual(3, q.dequeue().value)
        self.assertLess(q.length, 1)
        self.assertIs(q.first, q.last)

        self.assertIsNone(q.dequeue())
        self.assertIsNone(q.first)
        self.assertIsNone(q.last)



if __name__ == "__main__":
    unittest.main()
