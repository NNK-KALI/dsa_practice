import unittest
from dsa.core.Stack import Stack


class StackTest(unittest.TestCase):

    def test_constructor(self):
        s = Stack(1)

        self.assertIsNotNone(s.top)
        self.assertIsNone(s.top.next)
        self.assertEqual(1, s.top.value)
        self.assertEqual(1, s.height)

    def test_push(self):
        s = Stack(1)
        s.top = None
        s.height = 0

        s.push(2)
        self.assertEqual(s.top.value, 2)
        self.assertEqual(s.height, 1)
        self.assertIsNone(s.top.next)

        s.push(3)
        self.assertEqual(s.top.value, 3)
        self.assertEqual(s.height, 2)
        self.assertIsNotNone(s.top.next)

        s.push(4)
        self.assertEqual(s.top.value, 4)
        self.assertEqual(s.height, 3)

    def test_pop(self):
        s = Stack(1)
        s.push(2)
        s.push(3)

        self.assertEqual(3, s.pop().value)
        self.assertEqual(2, s.height)
        self.assertEqual(2, s.top.value)

        self.assertEqual(2, s.pop().value)
        self.assertEqual(1, s.height)
        self.assertEqual(1, s.pop().value)
        self.assertLess(s.height, 1)

        self.assertIsNone(s.pop())


if __name__ == "__main__":
    unittest.main(verbosity=2)
