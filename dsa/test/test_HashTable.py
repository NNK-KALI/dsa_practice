import unittest
from dsa.core.HashTable import HashTable


class TestHashTable(unittest.TestCase):
    def test_constructor(self):
        ht = HashTable()
        self.assertEqual([None] * 7, ht.data_map)
        del ht

        ht = HashTable(2)
        self.assertEqual([None] * 2, ht.data_map)

    def test_set_item(self):
        ht = HashTable()
        ht.set_item("bolts", 1400)
        self.assertEqual(ht.data_map[4], [['bolts', 1400]])
        ht.set_item("washers", 700)
        self.assertEqual(ht.data_map[4], [['bolts', 1400], ['washers', 700]])
        ht.set_item("lumber", 70)
        self.assertEqual([['lumber', 70]], ht.data_map[6])

        # ht.print_table()

    def test_get(self):
        ht = HashTable()
        ht.set_item("bolts", 1400)
        ht.set_item("washers", 700)
        ht.set_item("lumber", 70)
        # ht.print_table()

        self.assertEqual(1400, ht.get("bolts"))
        self.assertEqual(700, ht.get("washers"))
        self.assertEqual(70, ht.get("lumber"))
        self.assertIsNone(ht.get("nails"))

    def test_keys(self):
        ht = HashTable()
        ht.set_item("bolts", 1400)
        self.assertEqual(["bolts"], ht.keys())
        ht.set_item("washers", 700)
        self.assertEqual(["bolts", "washers"], ht.keys())
        ht.set_item("lumber", 70)
        # ht.print_table()

        self.assertNotEqual(["washers", "lumber"], ht.keys())
        self.assertEqual(["bolts", "washers", "lumber"], ht.keys())


if __name__ == "__main__":
    unittest.main(verbosity=2)
