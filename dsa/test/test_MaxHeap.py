import unittest
from dsa.core.MaxHeap import MaxHeap


class TestMaxHeap(unittest.TestCase):

    def test_constructor(self):
        max_heap = MaxHeap()
        self.assertListEqual([], max_heap.heap)

    def test_insert(self):
        max_heap = MaxHeap()
        max_heap.insert(99)
        self.assertListEqual([99], max_heap.heap)
        max_heap.insert(72)
        self.assertListEqual([99, 72], max_heap.heap)
        max_heap.insert(61)
        self.assertListEqual([99, 72, 61], max_heap.heap)
        max_heap.insert(58)
        self.assertListEqual([99, 72, 61, 58], max_heap.heap)
        max_heap.insert(100)
        self.assertListEqual([100, 99, 61, 58, 72], max_heap.heap)
        max_heap.insert(75)
        self.assertListEqual([100, 99, 75, 58, 72, 61], max_heap.heap)

    def test_remove(self):

        max_heap = MaxHeap()
        max_heap.insert(95)
        max_heap.insert(75)
        max_heap.insert(80)
        max_heap.insert(55)
        max_heap.insert(60)
        max_heap.insert(50)
        max_heap.insert(65)

        self.assertEqual(95, max_heap.remove())
        self.assertListEqual([80, 75, 65, 55, 60, 50], max_heap.heap)

        self.assertEqual(80, max_heap.remove())
        self.assertListEqual([75, 60, 65, 55, 50], max_heap.heap)


if __name__ == "__main__":
    unittest.main()
