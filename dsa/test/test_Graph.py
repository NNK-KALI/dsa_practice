import unittest
from dsa.core.Graph import Graph


class TestGraph(unittest.TestCase):
    def test_constructor(self):
        g = Graph()
        self.assertDictEqual({}, g.adj_list)

    def test_add_vertex(self):
        g = Graph()
        self.assertTrue(g.add_vertex("a"))
        self.assertDictEqual({"a": []}, g.adj_list)
        self.assertFalse(g.add_vertex("a"))
        self.assertTrue(g.add_vertex("b"))
        self.assertDictEqual({"a": [], "b": []}, g.adj_list)

    def test_add_edge(self):
        g = Graph()
        self.assertFalse(g.add_edge("a", "b"))
        self.assertTrue(g.add_vertex("a"))
        self.assertFalse(g.add_edge("a", "b"))
        self.assertTrue(g.add_vertex("b"))
        self.assertTrue(g.add_edge("a", "b"))
        self.assertDictEqual({"a": ["b"], "b": ["a"]}, g.adj_list)
        self.assertTrue(g.add_vertex("c"))
        self.assertTrue(g.add_edge("a", "c"))
        self.assertDictEqual({"a": ["b", "c"], "b": ["a"], "c": ["a"]}, g.adj_list)

    def test_remove_edge(self):
        g = Graph()
        self.assertTrue(g.add_vertex("A"))
        self.assertTrue(g.add_vertex("B"))
        self.assertTrue(g.add_vertex("C"))

        self.assertTrue(g.add_edge("A", "B"))
        self.assertTrue(g.add_edge("B", "C"))
        self.assertTrue(g.add_edge("C", "A"))

        self.assertDictEqual(
            {"A": ["B", "C"], "B": ["A", "C"], "C": ["B", "A"]}, g.adj_list
        )

        self.assertTrue(g.remove_edge("A", "B"))
        self.assertDictEqual({"A": ["C"], "B": ["C"], "C": ["B", "A"]}, g.adj_list)

        self.assertTrue(g.add_edge("A", "B"))
        self.assertDictEqual(
            {"A": ["C", "B"], "B": ["C", "A"], "C": ["B", "A"]}, g.adj_list
        )

        # Test for an isolated edge
        self.assertTrue(g.add_vertex("D"))

        self.assertTrue(g.remove_edge("A", "D"))
        self.assertDictEqual(
            {"A": ["C", "B"], "B": ["C", "A"], "C": ["B", "A"], "D": []}, g.adj_list
        )

    def test_remove_vertex(self):
        g = Graph()
        self.assertTrue(g.add_vertex("A"))
        self.assertTrue(g.add_vertex("B"))
        self.assertTrue(g.add_vertex("C"))
        self.assertTrue(g.add_vertex("D"))

        self.assertTrue(g.add_edge("A", "B"))
        self.assertTrue(g.add_edge("A", "C"))
        self.assertTrue(g.add_edge("A", "D"))
        self.assertTrue(g.add_edge("B", "D"))
        self.assertTrue(g.add_edge("C", "D"))

        self.assertDictEqual(
            {
                "A": ["B", "C", "D"],
                "B": ["A", "D"],
                "C": ["A", "D"],
                "D": ["A", "B", "C"],
            },
            g.adj_list,
        )

        self.assertTrue(g.remove_vertex("D"))
        self.assertDictEqual(
            {
                "A": ["B", "C"],
                "B": ["A"],
                "C": ["A"]
            }, g.adj_list
        )


if __name__ == "__main__":
    unittest.main()
