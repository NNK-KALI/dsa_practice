import unittest
from dsa.core.BST import BinarySearchTree


class TestBinarySearchTree(unittest.TestCase):
    def test_empty_constructor(self):
        bst = BinarySearchTree()

        self.assertIsNone(bst.root)

    def test_insert(self):
        bst = BinarySearchTree()

        self.assertTrue(bst.insert(2))
        self.assertEqual(2, bst.root.value)

        self.assertTrue(bst.insert(1))
        self.assertEqual(1, bst.root.left.value)
        self.assertIsNone(bst.root.left.left)
        self.assertIsNone(bst.root.left.right)

        self.assertTrue(bst.insert(3))
        self.assertEqual(3, bst.root.right.value)
        self.assertIsNone(bst.root.right.left)
        self.assertIsNone(bst.root.right.right)

        self.assertTrue(bst.insert(4))
        self.assertEqual(4, bst.root.right.right.value)
        self.assertIsNone(bst.root.right.right.left)
        self.assertIsNone(bst.root.right.right.right)

    def test_contains(self):
        bst = BinarySearchTree()

        self.assertFalse(bst.contains(1))

        bst.insert(2)
        bst.insert(1)
        self.assertTrue(bst.contains(1))
        bst.insert(3)
        bst.insert(4)

        self.assertTrue(bst.contains(1))
        self.assertTrue(bst.contains(2))
        self.assertTrue(bst.contains(3))
        self.assertTrue(bst.contains(4))
        self.assertFalse(bst.contains(5))


if __name__ == "__main__":
    unittest.main()
