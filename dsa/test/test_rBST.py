import unittest
from dsa.core.rBST import RecursiveBST


class TestRecursiveBST(unittest.TestCase):
    def test_empty_constructor(self):
        bst = RecursiveBST()

        self.assertIsNone(bst.root)

    def test_r_insert(self):
        bst = RecursiveBST()

        bst.r_insert(2)
        self.assertEqual(2, bst.root.value)

        bst.r_insert(1)
        self.assertEqual(1, bst.root.left.value)
        self.assertIsNone(bst.root.left.left)
        self.assertIsNone(bst.root.left.right)

        bst.r_insert(3)
        self.assertEqual(3, bst.root.right.value)
        self.assertIsNone(bst.root.right.left)
        self.assertIsNone(bst.root.right.right)

        bst.r_insert(4)
        self.assertEqual(4, bst.root.right.right.value)
        self.assertIsNone(bst.root.right.right.left)
        self.assertIsNone(bst.root.right.right.right)

    def test_r_contains(self):
        bst = RecursiveBST()

        self.assertFalse(bst.r_contains(1))

        bst.r_insert(2)
        bst.r_insert(1)
        print(bst.r_contains(1))
        self.assertTrue(bst.r_contains(1))
        bst.r_insert(3)
        bst.r_insert(4)

        self.assertTrue(bst.r_contains(1))
        self.assertTrue(bst.r_contains(2))
        self.assertTrue(bst.r_contains(3))
        self.assertTrue(bst.r_contains(4))
        self.assertFalse(bst.r_contains(5))


if __name__ == "__main__":
    unittest.main()
