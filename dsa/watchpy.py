import os
import subprocess
import hashlib
import time
import sys

file_to_monitor = sys.argv[1]
file_to_run = sys.argv[2]


def main():
    if os.path.isfile(file_to_monitor):
        prev_hash = None
        change_detected = True
        while True:
            sleeper(change_detected)
            try:
                with open(file_to_monitor, "rb") as f:
                    hash = hashlib.blake2b(f.read()).hexdigest()
                    if prev_hash != hash:
                        prev_hash = hash
                        cmd = ["python", file_to_run]
                        change_detected = True
                        os.system("clear")
                        print("-" * os.get_terminal_size().columns)
                        print(f"new hash: {hash}\n> {' '.join(cmd)}")
                        print("-" * os.get_terminal_size().columns)
                        subprocess.call(cmd)
                        print("-" * os.get_terminal_size().columns)
                    else:
                        change_detected = False
            except FileNotFoundError:
                pass

    else:
        print("404")
        exit()


wait_time = 0.2


def sleeper(flag):
    global wait_time
    if wait_time >= 2.0:
        wait_time = 0.2
    if flag is not True:
        wait_time = wait_time + 0.1
    time.sleep(wait_time)


if __name__ == "__main__":
    main()
